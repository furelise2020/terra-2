# Configure the AWS Provider
provider "aws" {
    region      = "ap-southeast-1"
    access_key  = "----------access key-----------"
    secret_key  = "----------secret key-----------"
}


# Create a VPC
resource "aws_vpc" "terra_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
      Name = "prod-vpc"
  }
}


# create Internet gateway
resource "aws_internet_gateway" "terra_gw" {
  vpc_id = aws_vpc.terra_vpc.id

  tags = {
    Name = "prod-gw"
  }
}


# create route table
resource "aws_route_table" "terra_rt" {
  vpc_id = aws_vpc.terra_vpc.id  

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terra_gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.terra_gw.id
  }

  tags = {
    Name = "prod-rt"
  }
}


# Create subnet
resource "aws_subnet" "terra_subnet" {
  vpc_id     = aws_vpc.terra_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "prod-subnet"
  }
}


# associate subnet with route table
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.terra_subnet.id
  route_table_id = aws_route_table.terra_rt.id
}


#create security group
resource "aws_security_group" "allow_web" {
  name        = "allow_web"
  description = "Allow WEB inbound traffic"
  vpc_id      = aws_vpc.terra_vpc.id

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "prod_web"
  }
}


#create network interface
resource "aws_network_interface" "web_server_nic" {
  subnet_id       = aws_subnet.terra_subnet.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]
}


#create elastic IP
resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.web_server_nic.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [aws_internet_gateway.terra_gw]
}


#create ubuntu server and install apache
resource "aws_instance" "terra-server" {
    ami             = "ami-0c8e97a27be37adfd"
    instance_type   = "t2.micro"
    availability_zone = "ap-southeast-1a"
    key_name = "yoyo"

    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.web_server_nic.id
    }

    user_data = <<-EOF
                #!/bin/bash
                sudo apt update -y
                sudo apt install apache2 -y
                sudo systemctl start apache2
                sudo bash -c 'echo your first web server > /var/www/html/index.html'
                EOF

    tags = {
        Name = "web-server"
    }
}

output "server_public_ip" {
    value = aws_eip.one.public_ip
}