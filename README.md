# Project Terraform

## Steps we will perform to configure our infra on AWS

1. Configure the AWS Provider
2. Create a VPC
3. create Internet gateway
4. create route table
5. Create subnet
6. associate subnet with route table
7. create security group
8. create network interface
9. create elastic IP
10. create ubuntu server and install apache

** we will have to add access_key and secret_key of our aws_user in step 1. [Even though it's not recommended to hardcode your credentials and keys] **

##### To check if infra is up, copy public IP in a browser tab to find out

### we can print public IP when we perform *terraform apply*

1.  Just include below in main.tf

    ```output "server_public_ip" {
        value = aws_eip.one.public_ip
    }
    ```

2.  output 
    
    ```Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
    Outputs:
    server_public_ip = 54.255.21.140
    ```

### other useful terraform commands wrt this project

#### 1. terraform state list

        ```
        aws_eip.one
        aws_instance.terra-server
        aws_internet_gateway.terra_gw
        aws_network_interface.web_server_nic
        aws_route_table.terra_rt
        aws_route_table_association.a
        aws_security_group.allow_web
        aws_subnet.terra_subnet
        aws_vpc.terra_vpc
        ```

#### 2. terraform state show <any line of previous output> e.g. terraform state show aws_eip.one

This gives us all the details of that resource.

        ```
        # aws_eip.one:
        resource "aws_eip" "one" {
            associate_with_private_ip = "10.0.1.50"
            association_id            = "eipassoc-0130d51cd265f5e6f"
            domain                    = "vpc"
            id                        = "eipalloc-064b259cb4e960d02"
            network_interface         = "eni-05686a58d8bbdcbe9"
            private_dns               = "ip-10-0-1-50.ap-southeast-1.compute.internal"
            private_ip                = "10.0.1.50"
            public_dns                = "ec2-54-255-21-140.ap-southeast-1.compute.amazonaws.com"
            public_ip                 = "54.255.21.140"
            public_ipv4_pool          = "amazon"
            vpc                       = true
        }
        ```

#### 3. terraform destroy

this will destroy entire infrastructure including all resources.

#### 4. terraform destroy -target aws_instance.terra-server

this will destroy only the web server

#### 5. terraform apply -target aws_instance.terra-server

this will apply only the web server

